## preamble
This applies to the 2021 and 2022 G14 G15 models and enables legacy suspend (S3) on those.

### important
You need to re-do those steps everytime you update or downgrade the BIOS!

When you update / downgrade the BIOS first boot without the acpi_override image, apply the script again and reboot.

## requirements
`acpica-tools pcre-tools`

## how to create modified DSDT for your G14 variant

- `git clone https://gitlab.com/marcaux/g14-2021-s3-dsdt.git`
- `cd g14-2021-s3-dsdt`
- `./modify-dsdt.sh`

## get acpi_override to load via GRUB

### Arch:
- edit `/etc/default/grub`

```
# GRUB_CMDLINE_LINUX_DEFAULT should already exist, so you have to add mem_sleep_default=deep to your existing line
# ... stands for the content you already have there, don't add ... ;)
GRUB_CMDLINE_LINUX_DEFAULT="mem_sleep_default=deep ..."

# GRUB_EARLY_INITRD_LINUX_CUSTOM has to be added, as it probably does not exist, yet
GRUB_EARLY_INITRD_LINUX_CUSTOM="acpi_override"
```

- regenerate grub
```
# grub-mkconfig -o /boot/grub/grub.cfg
```

### Fedora:
- edit `/etc/default/grub`

```
# GRUB_CMDLINE_LINUX should already exist, so you have to **add** mem_sleep_default=deep to your existing line
# ... stands for the content you already have there, don't add ... ;)
GRUB_CMDLINE_LINUX="mem_sleep_default=deep ..."

# GRUB_EARLY_INITRD_LINUX_CUSTOM has to be added, as it probably does not exist, yet
# not sure if this is needed still, see next step
GRUB_EARLY_INITRD_LINUX_CUSTOM="acpi_override"
```

- regenerate grub
```
# grub2-mkconfig -o /etc/grub2.cfg
```

- set the grubenv variable so it gets added dynamically for each entry

```
# grub2-editenv /boot/grub2/grubenv set early_initrd=acpi_override
```

### Fedora Silverblue:
- edit `/etc/default/grub`

```
# GRUB_EARLY_INITRD_LINUX_CUSTOM has to be added, as it probably does not exist, yet
GRUB_EARLY_INITRD_LINUX_CUSTOM="../../acpi_override"
```

- run `rpm-ostree kargs --editor`
- add `mem_sleep_default=deep` to the end of the existing options line so it could look like this (your existing parameters might look different):

```
# Current kernel arguments are shown below, and can be directly edited.
# Empty or commented lines (starting with '#') will be ignored.
# Individual kernel arguments should be separated by spaces, and the order 
# is relevant.
# Also, please note that any changes to the 'ostree=' argument will not be 
# effective as they are usually regenerated when bootconfig changes.

rhgb quiet root=UUID=bla-bli-blu rootflags=subvol=root mem_sleep_default=deep
```

- regenerate grub
```
# grub2-mkconfig -o /etc/grub2.cfg
```

## get acpi_override to load via systemd-boot

- add a loader to `/boot/loader/entries/` or edit an existing one
- add `initrd /acpi_override` before your Linux ramfs (likely the last `initrd` line)
- add `mem_sleep_default=deep` to your `options` line

as an example it could look like this:

```
title   Arch Linux
linux   /vmlinuz-linux
initrd  /acpi_override
initrd  /initramfs-linux.img
options root="LABEL=arch_os" rw mem_sleep_default=deep
```

## verify after cold boot

```
$ cat /sys/power/mem_sleep
s2idle [deep]
```
